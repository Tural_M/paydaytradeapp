import re
import httpx
from core.factories import settings
from app.controllers.controller.schemas import LoginSchema
from starlette.responses import JSONResponse
from typing import Union
from http import HTTPStatus
from core.extensions import log


def clean_dict(data):
    return {key: val for (key, val) in data.items() if val is not None}


async def login_check(login: LoginSchema) -> dict:
    try:

        check_result = await check_registration(login.email,login.password)
        token =  await request_to_auth(check_result.get("id"))
        check_result.update(token)
        check_result["stocks"] = await request_to_stock()
        return check_result

    except Exception as err:
        log.error(f"Error on login_check function ->  {err}", exc_info=True)
        return False


async def check_registration(
    email,
    password
):
    try:
        async with httpx.AsyncClient() as client:
            response_register = await client.post(
                f"{settings.REGISTER_SERVICE}/users/check",
                json={
                    "email": email,
                    "password": password
                })
            if response_register.status_code == 200:
                return response_register.json()
                    
    except Exception as err:
        log.error(f"Error on check_registration function ->  {err}",
                  exc_info=True)
        return False


async def request_to_auth(identity):
    try:
        async with httpx.AsyncClient() as client:
            response_token = await client.post(
                f"{settings.AUTH_SERVICE}/token/generator/default",
                json={"identity": identity})
            if response_token.status_code == 200:
                return response_token.json()
                

    except Exception as err:
        log.error(f"Error on request_to_auth function ->  {err}",
                  exc_info=True)
        return False

async def request_to_stock():
    try:
        async with httpx.AsyncClient() as client:
            response_token = await client.get(
                f"{settings.STOCK_SERVICE}/stocks")
            if response_token.status_code == 200:
                return response_token.json()
            return False
    except Exception as err:
        log.error(f"Error on request_to_auth function ->  {err}",
                  exc_info=True)
        return False

async def exit_user(token: str) -> JSONResponse:
    try:
        async with httpx.AsyncClient() as client:

            token = token.split(" ")[1]
            expire = await client.delete(
                f"{settings.AUTH_SERVICE}/token/generator/expire",
                headers={"Authorization": f"Bearer {token}"})

            if expire.status_code == 200:
                return True
        return False
    except Exception as identifier:
        log.error(f"Error on exit_user function ->  {identifier}",
                  exc_info=True)
        return False
