import pydantic.json
import asyncpg.pgproto.pgproto
from pydantic import BaseModel,constr,validator,ValidationError,EmailStr
from uuid import UUID
from typing import List,Union
from core.factories import settings


pydantic.json.ENCODERS_BY_TYPE[asyncpg.pgproto.pgproto.UUID] = str


class LoginSchema(BaseModel):
    email: EmailStr
    password: constr(regex=settings.PASSWORD_REGEX)
    
    @validator("email")
    def validate_email(cls,v):
        if v:
            return v.lower()
        return v
