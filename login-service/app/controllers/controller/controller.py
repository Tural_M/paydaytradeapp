from fastapi import APIRouter, Header, Body, Query
from starlette.responses import JSONResponse
from core.factories import settings
from app.utils.helpers import login_check
from app.utils.helpers import exit_user
from app.controllers.controller.schemas import LoginSchema
from core.factories import settings
from http import HTTPStatus

router = APIRouter()


 
@router.post("/users/login",tags=["Login Auth"],
response_description="Login",
description="Login",    
include_in_schema=settings.INCLUDE_SCHEMA)
async def user_login(login: LoginSchema) -> JSONResponse:


    user = await login_check(login)

    if user:
        return user
                    
    return JSONResponse(content={"result": False },status_code=HTTPStatus.NOT_FOUND)
    


@router.post("/users/logout",tags=["Login Auth"],
response_description="logout",
description="logout",    
include_in_schema=settings.INCLUDE_SCHEMA)
async def user_logout(Authorization = Header(...)) -> JSONResponse:


    result = await exit_user(Authorization)

    return JSONResponse(content={"result": result},status_code=HTTPStatus.OK)



    
  
