import coloredlogs
import logging
import os
from core.factories import settings

def get_logger(log_level="DEBUG"):
    if not settings.DEBUG:
        logger = logging.getLogger("gunicorn.error")
        logger.setLevel(log_level)   

    else:
        logger = logging.getLogger()
        colors_config = coloredlogs.DEFAULT_LEVEL_STYLES
        coloredlogs.DEFAULT_LOG_FORMAT = '%(asctime)s %(hostname)s %(name)s %(levelname)s %(message)s'
        colors_config.update(**{'info': {"color": "white","faint":True}})
        logger.setLevel(log_level)
        coloredlogs.install(level=log_level,logger=logger)

    return logger


log = get_logger(os.environ.get("LOG_LEVEL","DEBUG"))