from starlette.config import Config
from core.settings.settings import BaseConfig

class ProdSettings(BaseConfig):

    """ Configuration class for site development environment """

    config = Config()

    AUTH_SERVICE_HOST = config("AUTH_SERVICE_SERVICE_HOST")
    AUTH_SERVICE_PORT = config("AUTH_SERVICE_SERVICE_PORT", cast=int)
    AUTH_SERVICE = f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}"


    REGISTER_SERVICE_HOST =config("REGISTER_SERVICE_SERVICE_HOST")
    REGISTER_SERVICE_PORT = config("REGISTER_SERVICE_SERVICE_PORT", cast=int)
    REGISTER_SERVICE =  f"{REGISTER_SERVICE_HOST}:{REGISTER_SERVICE_PORT}"
    
    STOCK_SERVICE_HOST = config("STOCK_SERVICE_HOST")
    STOCK_SERVICE_PORT = config("STOCK_SERVICE_PORT", cast=int)
    STOCK_SERVICE =  f"{STOCK_SERVICE_HOST}:{STOCK_SERVICE_PORT}"