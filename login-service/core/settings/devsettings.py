from starlette.config import Config
from core.settings.settings import BaseConfig


class DevSettings(BaseConfig):

    """ Configuration class for site development environment """

    config = Config()

    
    DEBUG = config("DEBUG", cast=bool, default=True)
    
    AUTH_SERVICE_HOST = config("AUTH_SERVICE_HOST", default="http://127.0.0.1")
    AUTH_SERVICE_PORT = config("AUTH_SERVICE_PORT", cast=int, default=8000)
    AUTH_SERVICE = f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}"

    REGISTER_SERVICE_HOST =config("REGISTER_SERVICE_HOST", default="http://127.0.0.1")
    REGISTER_SERVICE_PORT = config("REGISTER_SERVICE_PORT", cast=int, default=8001)
    REGISTER_SERVICE =  f"{REGISTER_SERVICE_HOST}:{REGISTER_SERVICE_PORT}"

    STOCK_SERVICE_HOST = config("STOCK_SERVICE_HOST", default="http://127.0.0.1")
    STOCK_SERVICE_PORT = config("STOCK_SERVICE_PORT", cast=int, default=8002)
    STOCK_SERVICE =  f"{STOCK_SERVICE_HOST}:{STOCK_SERVICE_PORT}"
