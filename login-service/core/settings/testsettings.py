from starlette.config import Config
from core.settings.settings import BaseConfig


class TestSettings(BaseConfig):

    """ Configuration class for site testing environment """

    config = Config()

    
    DEBUG = config("DEBUG", cast=bool, default=True)
    
    AUTH_SERVICE_HOST = config("AUTH_SERVICE_HOST", default="http://localhost")
    AUTH_SERVICE_PORT = config("AUTH_SERVICE_PORT", cast=int, default=8020)
    AUTH_SERVICE = f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}"

    REGISTER_SERVICE_HOST =config("REGISTER_SERVICE_HOST", default="http://localhost")
    REGISTER_SERVICE_PORT = config("REGISTER_SERVICE_PORT", cast=int, default=8019)
    REGISTER_SERVICE =  f"{REGISTER_SERVICE_HOST}:{REGISTER_SERVICE_PORT}"
