## Services for Payday trade Aplication

##### List of available services 

###### auth service 
Service is responsible for autentication of the user using with JWT RSA256 algorithm.

###### login service
Service will be used for autenticate user to dashboard


###### signup service
Registers user in the platform with a email confirmation

###### stock service
This service will get stocks prices from http://api.marketstack.com/v1 API

###### Transaction service
Service will increase and decrease users balance

###### Order service
Service is responsible for placing an order