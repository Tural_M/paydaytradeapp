from starlette.config import Config
from starlette.datastructures import Secret,URL
from core.settings.settings import BaseConfig


class DevSettings(BaseConfig):

    """ Configuration class for site development environment """

    config = Config()

    DB_USER = config("DB_USER", cast=str, default="postgres")
    DB_PASSWORD = config("DB_PASSWORD", cast=Secret, default="postgres")
    DB_HOST = config("DB_HOST", cast=str, default="db")
    DB_PORT = config("DB_PORT", cast=str, default="5432")
    DB_NAME = config("DB_NAME", cast=str, default="postgres")

    DEBUG = config("DEBUG", cast=bool, default=True)
    INCLUDE_SCHEMA=config("INCLUDE_SCHEMA", cast=bool, default=True)
    JWT_PUBLIC_KEY = open(config("JWT_PUBLIC_KEY_PATH",cast=str),"rb").read()
    JWT_PRIVATE_KEY = open(config("JWT_PRIVATE_KEY_PATH",cast=str),"rb").read()

    INCLUDE_SCHEMA=config("INCLUDE_SCHEMA", cast=bool, default=True)
    ACCESS_TIMEDELTA = config("ACCESS_TIMEDELTA",cast=int,default=3)
    REFRESH_TIMEDELTA = config("REFRESH_TIMEDELTA",cast=int,default=5) 
    EMAIL_TIMEDELTA = config("EMAIL_TIMEDELTA",cast=int,default=5)

    DATABASE_URL = config(
        "DATABASE_URL",
        default=f"asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}",
    )  