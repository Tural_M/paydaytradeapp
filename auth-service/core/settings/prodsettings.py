from starlette.config import Config
from starlette.datastructures import Secret,URL
from core.settings.settings import BaseConfig
import os

class ProdSettings(BaseConfig):

    """ Configuration class for site development environment """

    config = Config()
    DEBUG = False
    INCLUDE_SCHEMA=config("INCLUDE_SCHEMA", cast=bool, default=False)
    CORS_ORIGINS = config("CORS_HOSTS")
    
    # Database envs
    DB_USER = config("DB_USER", cast=str)
    DB_PASSWORD = config("DB_PASSWORD", cast=Secret)
    DB_HOST = config("DB_HOST", cast=str)
    DB_PORT = config("DB_PORT", cast=str)
    DB_NAME = config("DB_NAME", cast=str)
    SSL_CERT_FILE = config("SSL_PATH")

    # Token envs
    ACCESS_TIMEDELTA = config("ACCESS_TIMEDELTA",cast=int,default=720)
    REFRESH_TIMEDELTA = config("REFRESH_TIMEDELTA",cast=int,default=720) 
    EMAIL_TIMEDELTA = config("EMAIL_TIMEDELTA",cast=int,default=180)
    JWT_ALGORITHM = "RS256" 
    JWT_PUBLIC_KEY = open(config("JWT_PUBLIC_KEY_PATH",cast=str),"rb").read()
    JWT_PRIVATE_KEY = open(config("JWT_PRIVATE_KEY_PATH",cast=str),"rb").read()

    
    DATABASE_URL = config(
        "DATABASE_URL",
        default=f"asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}",
    )
