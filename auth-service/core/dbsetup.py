from uuid import uuid4
from sqlalchemy.orm import relationship
from sqlalchemy_utils import UUIDType
from core.extensions import db
from sqlalchemy.sql import func

relationship = relationship
Column, Integer, String, BOOLEAN, ForeignKey, Datetime, Date,Text = (
    db.Column,
    db.Integer,
    db.String,
    db.BOOLEAN,
    db.ForeignKey,
    db.DateTime,
    db.Date,
    db.Text
)


class SurrogatePK:
    """A mixin that adds a surrogate UUID 'primary key' column named ``id`` to
    any declarative-mapped class."""

    __table_args__ = {"extend_existing": True}

    id = Column(UUIDType(binary=False), primary_key=True)
    created = Column(Datetime(timezone=True), default=func.now())
    updated = Column(Datetime(timezone=True), onupdate=func.now())

class Model(SurrogatePK, db.Model):
    __abstract__ = True


    @classmethod
    async def create(cls, **kwargs):
        if issubclass(cls, SurrogatePK):
            print(kwargs)
            unique_id = uuid4()
            if not kwargs.get("id"):

                kwargs["id"] = unique_id
        return await cls(**kwargs)._create()

