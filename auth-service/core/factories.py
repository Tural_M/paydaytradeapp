import os

envsettings = os.getenv("settings")

if envsettings == "dev" or envsettings == "default":
    from core.settings.devsettings import DevSettings
    settings = DevSettings()

elif envsettings == "prod":
    from core.settings.prodsettings import ProdSettings
    settings= ProdSettings()
elif envsettings == "test":
    from core.settings.testsettings import TestSettings
    settings= TestSettings()
else:
    from core.settings.settings import BaseConfig
    settings = BaseConfig()





