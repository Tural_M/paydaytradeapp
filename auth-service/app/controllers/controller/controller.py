from fastapi import (
    APIRouter,
    Header,
    Body,
    Path,
    Query,
)
from app.controllers.controller.schemas import TokenResponseSchema,TokenEmail
from uuid import UUID
from app.utils.helpers import (
    generate_token,
    extract_token,
    validate_token,
    jwt_identity,
    check_expiration,
    expire_token,
    create_access_token,
    extract_token,
    recreate_access_token
)
from typing import List
from starlette.responses import JSONResponse
from core.factories import settings
from datetime import timedelta
from http import HTTPStatus

router = APIRouter()


@router.post(
    "/generator/default",
    response_description="Create default token",
    description="",
    response_model=TokenResponseSchema,
    include_in_schema=settings.INCLUDE_SCHEMA,
    tags=["Token"]
)
async def create_token(identity: dict = Body(...,example={"identity" : "your_identity"})) -> JSONResponse:

    access,refresh = await generate_token(identity)
    return (
        JSONResponse(content={"access_token": access, "refresh_token": refresh, "result": True}, status_code=HTTPStatus.OK)
        if access and refresh
        else JSONResponse(content={"token": "","result" : False}, status_code=HTTPStatus.BAD_REQUEST)
    )

@router.post(
    "/generator/refresh",
    response_description="Create default token",
    description="",
    include_in_schema=settings.INCLUDE_SCHEMA,
    tags=["Token"]
)
async def refresh_token(Authorization: str =Header(...)) -> JSONResponse:
    token = extract_token(Authorization)
    access_token = await recreate_access_token(token)
    return (
        JSONResponse(content={"token": token, "result": True}, status_code=HTTPStatus.OK)
        if access_token
        else JSONResponse(content={"token": "","result" : False}, status_code=HTTPStatus.UNAUTHORIZED)
    )


@router.post(
    "/generator/email",
    response_description="Create token for email verification",
    description="",
    include_in_schema=settings.INCLUDE_SCHEMA,
    tags=["Token"]

)
async def create_token_for_email(identity: TokenEmail ) -> JSONResponse:

    token = await create_access_token(identity.dict(),expire_time=timedelta(minutes=settings.EMAIL_TIMEDELTA))
    return (
        JSONResponse(content={"token": token, "result": True}, status_code=HTTPStatus.OK)
        if token
        else JSONResponse(content={"token": "","result" : False}, status_code=HTTPStatus.UNAUTHORIZED)
    )


@router.delete(
    "/generator/expire",
    response_description="Expire tokens",
    description="",
    include_in_schema=settings.INCLUDE_SCHEMA,
    tags=["Token"]
)
async def expire_token_generator(Authorization: str = Header(...)) -> JSONResponse:
    token = extract_token(Authorization)
    result, identity = await expire_token(token)

    return JSONResponse(content={"result": result, "identity": identity}, status_code=HTTPStatus.OK if result else HTTPStatus.UNAUTHORIZED)
        

@router.get(
    "/generator/identity",
    response_description="Get user identity if token is valid",
    description="Extract identity from token ",
    include_in_schema=settings.INCLUDE_SCHEMA,
    tags=["Token"]
)
async def get_identity(
    token: str = Query(None),
    Authorization: str = Header(None)
) -> JSONResponse:

    if token:
        identity, result, status = await jwt_identity(token)
    else:
        token = extract_token(Authorization)
        identity, result, status = await jwt_identity(token)
        expire = await check_expiration(token) 

    return (
        JSONResponse(
            {
                "result": result,
                "identity": identity,
                "status": status,
                "message": "Success",
                "expire" : expire
            },
            status_code=HTTPStatus.OK,
        )
        if result
        else JSONResponse(
            {
                "result": result,
                "identity": identity,
                "status": status,
                "expire" : True,
                "message": "Not Found",
            },
            status_code=HTTPStatus.UNAUTHORIZED,
        )
    )


@router.get(
    "/generator/check",
    response_description="validate token",
    description="Validate token signature provided on header",
    include_in_schema=settings.INCLUDE_SCHEMA,
    tags=["Token"]
)
async def check_token(
    Authorization: str = Header(None), token=Query(None)
) -> JSONResponse:

    if Authorization:
        token = extract_token(Authorization)
        identity, result, _ = await validate_token(token)
    else:
        identity, result, _ = await validate_token(token)
    return (
        JSONResponse(content={"error": "Token validation error","result": result, "identity": identity}, status_code=HTTPStatus.UNAUTHORIZED)
        if not result
        else JSONResponse(
            content={"result": result, "identity": identity}, status_code=HTTPStatus.OK
        )
    )

