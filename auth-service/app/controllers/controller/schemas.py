# write your schemas in this files. Use pydantic

from pydantic import BaseModel, constr, validator, ValidationError, EmailStr
from uuid import UUID
from typing import Optional, List, Union, Mapping, Any, Dict
import pydantic.json
import asyncpg.pgproto.pgproto
from datetime import datetime

pydantic.json.ENCODERS_BY_TYPE[asyncpg.pgproto.pgproto.UUID] = str


class TokenSchema(BaseModel):

    identity: str
    token: str
    type:str
    expire_time: datetime


class TokenSchemaInDb(TokenSchema):
    id: UUID

    class Config:
        orm_mode = True


class TokenResponseSchema(BaseModel):
    acces_token: str
    refresh_token: str
    result: bool


class TokenEmail(BaseModel):
    identity: EmailStr

