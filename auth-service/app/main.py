import random
import string
import time

from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette_prometheus import PrometheusMiddleware, metrics
from app.controllers.controller.controller import router
from app.data.models import *  # noqa F401
from app.utils.helpers import check_protected_path
from core.extensions import db  # noqa F401
from core.extensions import log
from core.factories import settings

app = FastAPI()
db.init_app(app)


@app.middleware("http")
async def log_requests(request: Request, call_next):
    rid = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
    log.info(
        f"RID={rid} START REQUEST PATH={request.url.path} METHOD={request.method} "
    )
    start_time = time.time()
    response = await call_next(request)

    process_time = (time.time() - start_time) * 1000
    formatted_process_time = '{0:.2f}'.format(process_time)
    log.info(
        f"RID={rid} COMPLETED={formatted_process_time}ms REQUEST={request.method.upper()} {request.url.path} STATUS_CODE={response.status_code}"
    )

    return response


def modify_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Auth Service",
        version="1.0.0",
        description=
        "This OpenAPI schema for generate and check acces tokens for all microservices",
        routes=app.routes,
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = modify_openapi


@app.on_event("startup")
async def startup():
    print("app started succes")


@app.on_event("shutdown")
async def shutdown():
    print("SHUTDOWN APP")


class CustomHeaderMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        if await check_protected_path(request.url.path):
            if ("authorization" in request.headers.keys()
                    or "token" in request.query_params.keys()):
                response = await call_next(request)
                return response
            else:
                return JSONResponse({"error": "access token not found"},
                                    status_code=401)
        response = await call_next(request)
        return response


app.add_middleware(CustomHeaderMiddleware)

cors_origins = [i.strip() for i in settings.CORS_ORIGINS.split(",")]
app.add_middleware(
    CORSMiddleware,
    allow_origins=cors_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics/", metrics)
app.include_router(router, prefix="/token")
