# write you database models in this file

from core.dbsetup import (
    Column,
    Model,
    UUIDType,
    relationship,
    String,
    Integer,
    ForeignKey,
    BOOLEAN,
    Datetime,
    Date,
    Text,
)
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.sql import func


class TokenSessions(Model):
    __tablename__ = "token_session"

    token = Column(String(), unique=True, nullable=False)
    expire_time = Column(Datetime(timezone=True), nullable=False)
    identity = Column(String(), nullable=False, index=True)
    type = Column(String(),nullable=True)
    status = Column(BOOLEAN(), nullable=False, default=True)
