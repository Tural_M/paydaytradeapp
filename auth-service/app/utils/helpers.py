import jwt
import hashlib
from core.factories import settings
from datetime import datetime, timedelta
from app.data.models import TokenSessions
from fastapi import Header
from app.controllers.controller.schemas import TokenSchema
from app.controllers.controller.schemas import TokenSchemaInDb
from core.extensions import log
from sqlalchemy import and_
from core.extensions import db


def extract_token(data: str) -> str:
    """ Extract JWT token from header """
    if len(data.split()) >=2:
        return data.split()[1]

async def check_protected_path(path: str) -> bool:
    if path == "/token/generator/email" or path == "/token/generator/default" or path == "/docs" or path == "/openapi.json":
        return False
    return True


def get_token_hash(token: str):
    if token and isinstance(token, str):
        return hashlib.sha256(token.encode()).hexdigest()


def clean_dict(data: dict) -> dict:
    return {key: val for (key, val) in data.items() if val is not None}


async def decode_token(token: str):
    try:
        log.info("START decode_token FUNCTION")
        return jwt.decode(token,
                          settings.JWT_PUBLIC_KEY,
                          algorithms=[settings.JWT_ALGORITHM])
    except (
            jwt.InvalidSignatureError,
            jwt.DecodeError,
            jwt.ExpiredSignatureError,
            jwt.InvalidAlgorithmError,
    ) as err:
        log.error(f"Error on decode_token function ->  {err}")
        return False


async def create_access_token(
    identity: dict,
    expire_time: timedelta = timedelta(minutes=settings.ACCESS_TIMEDELTA)
) -> str:
    """ Generate new access token for users  and save on db"""
    try:
        log.info("START create_access_token FUNCTION")
        if identity.get("identity"):
            identity["exp"] = datetime.utcnow() + expire_time
            identity["type"] = "access"
            token = jwt.encode(
                identity,
                settings.JWT_PRIVATE_KEY,
                algorithm=settings.JWT_ALGORITHM).decode("utf-8")
            exp = _epoch_utc_to_datetime(identity["exp"])
            await save_token(token=get_token_hash(token),
                             expire_time=exp,
                             identity=identity.get("identity"),
                             type=identity.get("type"))
            log.info("END create_access_token FUNCTION")

            return token
    except Exception as err:
        log.error(f"Error on create_access_token function ->  {err}")
        return False


async def create_refresh_token(
    identity: dict,
    expire_time: timedelta = timedelta(minutes=settings.REFRESH_TIMEDELTA)
) -> str:
    """ Generate new refresh token for users  and save on db"""
    try:
        log.info("START create_refresh_token FUNCTION")

        if identity.get("identity"):
            identity["exp"] = datetime.utcnow() + expire_time
            identity["type"] = "refresh"
            token = jwt.encode(
                identity,
                settings.JWT_PRIVATE_KEY,
                algorithm=settings.JWT_ALGORITHM).decode("utf-8")
            exp = _epoch_utc_to_datetime(identity["exp"])
            await save_token(token=get_token_hash(token),
                             expire_time=exp,
                             identity=identity.get("identity"),
                             type=identity.get("type"))
            log.info("END create_refresh_token FUNCTION")
            return token
    except Exception as err:
        log.error(f"Error on create_refresh_token function ->  {err}")
        return None


async def generate_token(identity: dict) -> str:
    """ Generate new access token for users  and save on db"""
    try:
        log.info("START generate_token FUNCTION")

        if identity:
            access = await create_access_token(identity)
            refresh = await create_refresh_token(identity)
            log.info("END generate_token FUNCTION")

            return access, refresh
    except Exception as err:
        log.error(f"Error on generate_token function ->  {err}")
        return None, None


async def recreate_access_token(
    refresh_token: str, expire_time: timedelta = timedelta(minutes=3)) -> str:
    """ Generate new refresh token for users  and save on db"""
    try:
        log.info("START recreate_access_token FUNCTION")

        identity = jwt.decode(refresh_token,
                              settings.JWT_PUBLIC_KEY,
                              algorithms=[settings.JWT_ALGORITHM])

        if identity.get("type") != "refresh":
            log(f"INCORRECT TOKEN TYPE -> {identity.get('type')}")
            log.info("END recreate_access_token FUNCTION")

            return None
        token = await create_access_token(
            identity={"identity": identity.get("identity")})
        log.info("END recreate_access_token FUNCTION")

        return token
    except Exception as err:
        log.error(f"Error on recreate_access_token function ->  {err}")
        return None


async def check_expiration(token: str) -> bool:
    try:
        log.info("START check_expiration FUNCTION")

        jwt.decode(token,
                   settings.JWT_PUBLIC_KEY,
                   algorithms=[settings.JWT_ALGORITHM])
        log.info("END check_expiration FUNCTION")

        return False
    except jwt.ExpiredSignatureError:
        return True
    except Exception as err:
        log.error(f"Error on check_expiration function ->  {err}")
        return True


async def jwt_identity(token: str) -> str:
    """ decode raw data from JWT token and return identity, query result, status """

    log.info("START jwt_identity FUNCTION")
    identity, result, status = await validate_token(token)
    return (identity, result, status)


async def expire_token(token):
    log.info("START expire_token FUNCTION")

    identity = await decode_token(token)
    if identity is False:
        log.info(f"INCORRECT IDENTITY -> {identity}")
        return False, None
    try:

        token_info = await TokenSessions.query.where(
            and_(TokenSessions.identity == identity.get("identity"),
                 TokenSessions.token == get_token_hash(token),
                 TokenSessions.status == True, 
                 TokenSessions.type == identity.get("type"))).gino.first()
        if token_info:
            async with db.transaction():
                await token_info.update(status=False).apply()
                log.info("END expire_token FUNCTION")

                return True, token_info.identity

        log.info("END expire_token FUNCTION")
        return False, None
    except Exception as err:
        log.error(f"Error on expire_token function ->  {err}")
        
        return False, None


async def save_token(**kwargs: dict) -> str:
    """ Save token info in  db """

    log.info("START save_token FUNCTION")

    token_info = TokenSchema(**kwargs)
    prev_token = await TokenSessions.query.where(
        and_(TokenSessions.identity == token_info.identity,
             TokenSessions.type == token_info.type)).gino.first()

    async with db.transaction():
        if prev_token is None:
            await TokenSessions.create(**token_info.dict())
        else:
            await prev_token.update(token=token_info.token,
                                    expire_time=token_info.expire_time,
                                    status=True).apply()
    log.info("END save_token FUNCTION")


async def validate_token(token: str) -> tuple:
    """ Validate token signature and expire time """

    try:
        log.info("START validate_token FUNCTION")
        identity = await decode_token(token)
        if identity is not False:
            token_in_db = await TokenSessions.query.where(
                and_(TokenSessions.identity == identity.get("identity"),
                     TokenSessions.status == True,
                     TokenSessions.type == identity.get("type"))).gino.first()
            if token_in_db:
                if token_in_db.token == get_token_hash(
                        token) and identity.get("identity") == token_in_db.identity :
                    log.info("END validate_token FUNCTION")
        
                    return (token_in_db.identity, True, token_in_db.status)
        log.info("END validate_token FUNCTION")

        return (None, False, False)
    except Exception as err:
        log.error(f"Error on validate_token function ->  {err}")
        return (None, False, False)


def _epoch_utc_to_datetime(token_expire):
    """
    Helper function for converting epoch timestamps (as stored in JWTs) into
    python datetime objects .
    """
    return datetime.fromtimestamp(token_expire)
