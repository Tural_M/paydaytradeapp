import pytest
from app.utils.helpers import decode_token
from app.main import app
from httpx import AsyncClient

identity = "Tural"

@pytest.mark.asyncio
async def test_create_token(client):
    
    async with AsyncClient(app=app, base_url="http://127.0.0.1/token/generator") as client:
        response = await client.post("/default",json={"identity": identity})
        assert response.status_code == 200
        assert response.json().keys() == dict(access_token="",refresh_token="",result = True).keys()
        check_record = await client.get("/identity",headers={"Authorization": f"Bearer {response.json().get('access_token')}"})
        assert check_record.status_code == 200
        assert check_record.json() == {
                                        "result": True,
                                        "identity": identity,
                                        "status": True,
                                        "message": "Success",
                                        "expire": False
                                    }

        check_record = await client.get("/check",headers={"Authorization": f"Bearer {response.json().get('access_token')}"})
        assert check_record.status_code == 200
        assert check_record.json() == {
                                        "result": True,
                                        "identity": identity
                                    }

        check_record = await client.delete("/expire",headers={"Authorization": f"Bearer {response.json().get('access_token')}"})
        assert check_record.status_code == 200
        assert check_record.json() == {
                                        "result": True,
                                        "identity": identity
                                    }

        check_record = await client.get("/identity",headers={"Authorization": f"Bearer {response.json().get('access_token')}"})
        assert check_record.status_code == 401
        assert check_record.json() == {
                                        "result": False,
                                        "identity": None,
                                        "status": False,
                                        "expire": True,
                                        "message": "Not Found"
                                    }    


        check_record = await client.get("/check",headers={"Authorization": f"Bearer {response.json().get('access_token')}"})
        assert check_record.status_code == 401
        assert check_record.json() == {
                                        "result": False,
                                        "identity": None,
                                        "error": "Token validation error",
                                    }    



