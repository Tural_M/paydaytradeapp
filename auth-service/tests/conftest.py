import sys
import os
import pytest
from alembic.config import main
from starlette.config import environ
from starlette.testclient import TestClient
from core.factories import settings


# environ["TESTING"] = "TRUE"

@pytest.fixture
def client():
    from app.main import app
    main(["--raiseerr", "upgrade", "head"])

    with TestClient(app) as client:
        yield client

    main(["--raiseerr", "downgrade", "base"])

@pytest.fixture
def test_settings():
    return settings