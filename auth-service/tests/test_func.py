import pytest
from app.utils.helpers import decode_token
from app.utils.helpers import create_access_token
from app.utils.helpers import create_refresh_token
from app.utils.helpers import expire_token
from app.utils.helpers import validate_token
from app.utils.helpers import extract_token


@pytest.mark.asyncio
async def test_create_access_token(client):
    identity = {"identity" : "testuser"}
    token = await create_access_token(identity)
    assert isinstance(token,str)
    

    identity = {"identity" : 123}
    token = await create_access_token(identity)
    assert isinstance(token,str)

    identity = {"identity" : ""}
    token = await create_access_token(identity)
    assert token is  None

    identity = {"another_identity" : "testuser"}
    token = await create_access_token(identity)
    assert token is  None

@pytest.mark.asyncio
async def test_create_refresh_token(client):
    identity = {"identity" : "testuser"}
    token = await create_refresh_token(identity)
    assert isinstance(token,str)
    

    identity = {"identity" : 123}
    token = await create_refresh_token(identity)
    assert isinstance(token,str)

    identity = {"identity" : ""}
    token = await create_refresh_token(identity)
    assert token is  None

    identity = {"another_identity" : "testuser"}
    token = await create_refresh_token(identity)
    assert token is  None

@pytest.mark.asyncio
async def test_decode_token(client):
    token = "jwt_token"
    result = await decode_token(token)

    assert result == False

    token = 12334242
    result = await decode_token(token)

    assert result is False

    identity = {"identity" : "testuser"}
    token = await create_access_token(identity)

    dec_token = await decode_token(token)

    assert dec_token.get("identity") == identity.get("identity")
    
@pytest.mark.asyncio
async def test_token_types(client):
    identity = {"identity" : "testuser"}
    token = await create_access_token(identity)
    result = await decode_token(token)
    assert result.get("type") == "access"

    token = await create_refresh_token(identity)
    result = await decode_token(token)
    assert result.get("type") == "refresh"


@pytest.mark.asyncio
async def test_expire_token(client):
    identity = {"identity" : "testuser"}
    token = await create_access_token(identity)
    result, temp_identity = await expire_token(token)
    assert result == True
    assert temp_identity == identity.get("identity")

    identity, result, status = await validate_token(token)

    assert identity is None
    assert result is False
    assert status is False


def test_extract_token():
    token = "jwt_token"
    result = extract_token(token)
    assert result is None

    header = "Bearer test_token"
    token = extract_token(header)
    assert token == "test_token"

