from fastapi import APIRouter
from fastapi import Query
from fastapi import Path
from fastapi import BackgroundTasks
from starlette.responses import JSONResponse
from app.utils.helpers import get_list_of_stocks
from app.utils.helpers import get_stock_by_name
from core.factories import settings
from http import HTTPStatus


router = APIRouter()


@router.get("/stocks",
            tags=["stocks"],
            response_description="",
            description="Get all stock",
            include_in_schema=settings.INCLUDE_SCHEMA,
)
async def get_stocks() -> JSONResponse:
    result =  await get_list_of_stocks()
    if result: return result
    else:
        return JSONResponse(content={
                            "result": False,
                            "message": "Failure",
                            "error": f"Error happened",
                        },status_code=HTTPStatus.NOT_FOUND)


@router.get("/stocks/{symbol}",
            tags=["stocks"],
            response_description="",
            description="Get symbol info from stock",
            include_in_schema=settings.INCLUDE_SCHEMA,
)
async def get_stock(symbol: str = Path(...), date: str = Query("latest")) -> JSONResponse:
    result =  await get_stock_by_name(symbol=symbol, date=date)
    if result: return result
    else:
        return JSONResponse(content={
                            "result": False,
                            "message": "Failure",
                            "error": f"Error happened",
                        }, status_code=HTTPStatus.NOT_FOUND)


