import httpx
import time
from core.extensions import log
import asyncio
from core.factories import settings


tickers = ["MSFT", "AAPL", "AMZN"]


async def get_list_of_stocks(date:str = "latest"):
    return await get_stocks(date)
    

async def get_stocks(date:str = "latest", symbols:list = tickers):
    temp = dict()
    try:
        async with httpx.AsyncClient() as client:
            symbols = ",".join(symbols)
            response = await client.get(f"http://api.marketstack.com/v1/eod/{date}?access_key={settings.API_ACCESS_KEY}&symbols={symbols}")
            if response.status_code == 200 and response.json().get("data"):
                for value in response.json().get("data"):
                    temp[value.get("symbol")] = value 
        return temp
    except Exception as err:
        log.error(f"Error on get_stocks function ->  {err}")
        return False
 
async def get_stock_by_name(symbol: str,date: str = "latest"):
    return await get_stocks(date = date, symbols=[symbol])
    