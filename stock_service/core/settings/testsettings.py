from starlette.config import Config
from core.settings.settings import BaseConfig


class TestSettings(BaseConfig):

    """ Configuration class for site test environment """

    config = Config()

    API_ACCESS_KEY = config("API_ACCESS_KEY", cast=str, default="53716e49b0145d7ef8009de9352d6601")
    
    DEBUG = config("DEBUG", cast=bool, default=True)
