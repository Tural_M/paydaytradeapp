from starlette.config import Config
from starlette.datastructures import Secret
import os

project_name="signup_service"


class BaseConfig:

    """
    Base configuration class. Subclasses should include configurations for
    testing, development and production environments
    """
    config = Config()

    INCLUDE_SCHEMA=config("INCLUDE_SCHEMA", cast=bool, default=True)

    LOGGER_NAME = "%s_log" % project_name
    LOG_FILENAME = "/var/tmp/app.%s.log" % project_name

    CORS_ORIGINS = config("CORS_HOSTS",default="*")

    DEBUG = config("DEBUG", cast=bool, default=False)
    TESTING = config("TESTING", cast=bool, default=False)

