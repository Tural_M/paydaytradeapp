import os
from starlette.config import Config
from starlette.datastructures import Secret,URL
from core.settings.settings import BaseConfig

class ProdSettings(BaseConfig):

    """ Configuration class for site prodaction environment """

    config = Config()
    DEBUG = False
    INCLUDE_SCHEMA=config("INCLUDE_SCHEMA", cast=bool, default=False)
    
    API_ACCESS_KEY = config("API_ACCESS_KEY", cast=str)
    
