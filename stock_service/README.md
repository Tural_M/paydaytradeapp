# Python FastAPI Stock Service 



## Run tests ##
```sh
> export settings=test

> pytest --cov-report term --cov=app tests/
```

## Usage Examples ##


```sh
> pipenv shell or python3 -m venv .ven 
> pipenv install or pip install -r requirements.txt
> export settings=dev

> uvicorn app.main:app --reload --port 8002
```
###### after app is runing, go to http://127.0.0.1:8002/docs to see docs
