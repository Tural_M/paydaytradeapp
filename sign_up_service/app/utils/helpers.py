import datetime
import httpx
import re
from core.dbsetup import and_
from fastapi_mail import FastMail
from fastapi_mail import MessageSchema
from starlette.responses import JSONResponse
from http import HTTPStatus
from core.factories import settings
from core.extensions import db
from core.extensions import log
from core.extensions import email_conf
from app.data.models import Users
from app.controllers.controller.schemas import UserSchemaDB
from app.controllers.controller.schemas import UserSchema
from app.controllers.controller.schemas import CheckUserSchema
from app.controllers.controller.schemas import get_secret_hash
from app.controllers.controller.schemas import verify_secret
from app.utils.templates import email_verification_template


async def register_user(background_tasks, data: dict):
    """ Save new user and sent email confirmation link """
    try:
        exist_user = await Users.query.where(and_(Users.email == data.get('email'), Users.confirm_status == True)).gino.first()
        if exist_user:
            return JSONResponse(content={"msg": "Email already registered"}, status_code=HTTPStatus.BAD_REQUEST)
        message = await create_message_schema(data)
        if message:
            await save_user(data)
            fm = FastMail(email_conf)
            background_tasks.add_task(fm.send_message, message)
            return await success_response(data)
        return False
    except Exception as e:
        log.error(f"Error on register_user function ->  {e}")
        return False


async def send_email_notif(background_tasks, email):
    """ Send email notification about purchase """
    try:
        user = await Users.query.where(Users.email == email).gino.first()
        if user:
            message = MessageSchema(
                subject="purchase",
                recipients=[email],
                body="Success",
                subtype="text"
            )
            fm = FastMail(email_conf)
            background_tasks.add_task(fm.send_message, message)
            return JSONResponse(content={"msg": "success"}, status_code=HTTPStatus.OK)
    except Exception as e:
        log.error(f"Error on register_user function ->  {e}")
        return False


async def create_template(token: str):
    """ Create template for email body """
    current_date = await get_date()
    template = email_verification_template.format(
        f'{settings.HOST}/users/register/confirm?token={token}', current_date)

    return template


async def create_message_schema(data: dict):
    """ Create message for email """
    try:
        token = await generate_key(data.get("email"))
        if token:
            template = await create_template(token)
            message = MessageSchema(
                subject="Complete Registration for Payday Trade",
                recipients=[data.get("email")],
                body=template,
                subtype="html")
            return message

        return False
    except Exception as err:
        log.error(f"Error on create_template function ->  {err}")
        return False


async def generate_key(email: str):
    """ Get activation token from auth service """
    try:
        async with httpx.AsyncClient() as client:
            response = await client.post(
                f"{settings.AUTH_SERVICE}/token/generator/email",
                json={"identity": email})

            if response.status_code == 200:
                response = response.json().get("token")
                return response
            return False

    except Exception as e:
        log.error(f"Error on generate_key function ->  {e}")
        return False


async def get_date():
    return str(datetime.datetime.now().strftime("%d %B %Y, %H:%M:%S"))


async def not_valid_email_response(email):
    return JSONResponse(content={
            "result": False,
            "message": "Failure",
            "error": f"Given mail address {email} is not valid",
        },
        status_code=HTTPStatus.NOT_FOUND,
    )


async def success_response(data):

    return JSONResponse(content={
            "message":
            f"email has been sent {data.get('email')}, please check your mail address"
        },
        status_code=HTTPStatus.OK,
    )


async def confirm_user(token: str):
    """ Confirm user from email activation link """
    try:
        identity = await check_token(token)
        print(identity)
        if not identity:
            return False
        status = await change_status_user(identity,token)
        return status if status else False
    except Exception as err:
        log.error(f"Error on confirm_user function ->  {err}")
        return False


async def change_status_user(identity: str,token: str):
    """ Change activated user status """
    try:
        async with db.transaction():
            user = await Users.query.where(Users.email == identity
                                           ).gino.first()
            if not user:
                return False
            await user.update(confirm_status=True).apply()
            expired_identity = await expire_token(token)
            if expired_identity == identity:
                return UserSchemaDB.from_orm(user)

            raise Exception("incorrect identity")
    except Exception as err:
        log.error(f"Error on change_status_user function ->  {err}")
        return False


async def expire_token(token):
    """ Manual expire activation token after user confirmed """
    try:
        async with httpx.AsyncClient() as client:
            response = await client.delete(
                f"{settings.AUTH_SERVICE}/token/generator/expire",
                headers={"Authorization": f"Bearer {token}"},
            )
            if response.status_code == 200:
                return response.json().get("identity")
            return False

    except Exception as err:
        log.error(f"Error on expire_token function ->  {err}")
        return False


async def check_token(token):
    """ check activation token """
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                f"{settings.AUTH_SERVICE}/token/generator/identity",
                headers={"Authorization": f"Bearer {token}"},
            )
            if response.status_code == 200:
                return response.json().get("identity")
            return False

    except Exception as err:
        log.error(f"Error on check_token function ->  {err}")
        return False


async def save_user(data: dict):
    try:
        async with db.transaction():
            user = await Users.query.where(Users.email == data.get("email")
                                           ).gino.first()
            if not user:
                data['password'] = get_secret_hash(data.get("password"))
                await Users.create(**data)
            

    except Exception as err:
        log.error(f"Error on save_user function ->  {err}")
        return False


async def check_user(data: CheckUserSchema):
    try:
        user = await Users.query.where(Users.email == data.email).gino.first()
        if user and verify_secret(data.password, user.password):
            return UserSchemaDB.from_orm(user)
        return False
    except Exception as err:
        log.error(f"Error on check_user function ->  {err}")
        return False
