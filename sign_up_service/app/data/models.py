from core.dbsetup import Column
from core.dbsetup import Model
from core.dbsetup import String
from core.dbsetup import BOOLEAN


class Users(Model):
    __tablename__ = "users"

    name = Column(String(), nullable=False)
    email = Column(String(),nullable=False, index=True)
    password = Column(String(),nullable=False)
    confirm_status = Column(BOOLEAN(), default=False)

