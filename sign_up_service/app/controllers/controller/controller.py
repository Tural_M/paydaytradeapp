from fastapi import APIRouter
from fastapi import Query
from fastapi import Body
from fastapi import BackgroundTasks
from starlette.responses import JSONResponse
from http import HTTPStatus
from core.factories import settings
from app.controllers.controller.schemas import UserSchema
from app.controllers.controller.schemas import CheckUserSchema
from app.controllers.controller.schemas import EmailStr
from app.utils.helpers import register_user
from app.utils.helpers import confirm_user
from app.utils.helpers import check_user
from app.utils.helpers import send_email_notif


router = APIRouter()


@router.post("/users/register",
            tags=["Register"],
            response_description="",
            description="Register Users",
            include_in_schema=settings.INCLUDE_SCHEMA,
)
async def registartion(background_tasks: BackgroundTasks,data: UserSchema) -> JSONResponse:

    result = await register_user(background_tasks, data.dict()) 

    if result:
        return result

    return JSONResponse( content={
                            "result": False,
                            "message": "Failure",
                            "error": f"Error happened",
                        }, status_code=HTTPStatus.NOT_FOUND)



@router.get("/users/register/confirm",
            tags=["Register"],
            response_description="Sign Up Users",
            description="Sign Up Users",
            include_in_schema=settings.INCLUDE_SCHEMA,
)
async def confirmation(token= Query(..., alias="token", title="token", description="Send token in the query")) -> JSONResponse:
    result = await confirm_user(token)
    if result: return result
    return JSONResponse(content={
                            "result": False,
                            "message": "Failure",
                            "error": f"Error happened",
                        },status_code=HTTPStatus.NOT_FOUND)
  

@router.post("/users/check",
            tags=["Register"],
            response_description="",
            description="Register Users",
            include_in_schema=settings.INCLUDE_SCHEMA,
)
async def check(data: CheckUserSchema) -> JSONResponse:
    result = await check_user(data)
    if result:
        return result
    return JSONResponse(content={"msg": "email or password incorrect"},status_code=HTTPStatus.NOT_FOUND)


@router.post("/users/notification",
            tags=["Register"],
            response_description="",
            description="Notify Users",
            include_in_schema=settings.INCLUDE_SCHEMA,
)
async def send_notification(background_tasks: BackgroundTasks, email: EmailStr = Body(..., embed=True)) -> JSONResponse:
    result = await send_email_notif(background_tasks, email)
    if result:
        return result
    return JSONResponse(content={"msg": "email or password incorrect"},status_code=HTTPStatus.NOT_FOUND)