import pydantic.json
import asyncpg.pgproto.pgproto
from datetime import datetime
from uuid import UUID
from core.factories import settings
from typing import Union
from pydantic import (
    BaseModel,
    constr,
    validator,
    EmailStr,
)
from core.extensions import pwd_context
from core.extensions import log


pydantic.json.ENCODERS_BY_TYPE[asyncpg.pgproto.pgproto.UUID] = str


def validate_str_empty(text: str):
    
    if text.isspace():
        return False
    elif len(text.strip()) == 0:
        return False
    return text.strip()


def get_secret_hash(password: str):
    """ Generate password hash """
    log.info("generate password hash")
    return pwd_context.hash(password)

def verify_secret(plain_pass: str, hashed_pass: str):
    """ Check user  password and hashed password in db"""
    log.info("verify password")
    return pwd_context.verify(plain_pass, hashed_pass)


class UserSchema(BaseModel):
    name: str
    email: EmailStr
    password: constr(regex=settings.PASSWORD_REGEX)

    @validator("name")
    def validate_name(cls,v):
        v = validate_str_empty(v)
        if v:
            return v
        raise ValueError("invalid usernname")

    @validator("email")
    def validate_email(cls,v):
        if v:
            return v.lower()
        return v


class CheckUserSchema(BaseModel):
    email: EmailStr
    password: constr(regex=settings.PASSWORD_REGEX)

    @validator("email")
    def validate_email(cls,v):
        if v:
            return v.lower()
        return v


class UserSchemaDB(BaseModel):
    id: UUID
    name: str
    email: EmailStr
    created: datetime
    updated: Union[None, datetime] = None

    class Config:
        orm_mode = True

