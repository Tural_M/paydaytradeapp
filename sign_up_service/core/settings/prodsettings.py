import os
from starlette.config import Config
from starlette.datastructures import Secret,URL
from core.settings.settings import BaseConfig

class ProdSettings(BaseConfig):

    """ Configuration class for site prodaction environment """

    config = Config()

    HOST = config("EMAIL_CONFIRM_HOST")

    DB_USER = config("DB_USER", cast=str,)
    DB_PASSWORD = config("DB_PASSWORD", cast=Secret)
    DB_HOST = config("DB_HOST", cast=str)
    DB_PORT = config("DB_PORT", cast=str)
    DB_NAME = config("DB_NAME", cast=str)
    INCLUDE_SCHEMA=config("INCLUDE_SCHEMA", cast=bool, default=False)
    SSL_CERT_FILE = config("SSL_PATH")
    
    MAIL_USERNAME = config("MAIL_USERNAME", cast=str)
    MAIL_PASSWORD = config("MAIL_PASSWORD", cast=str)
    MAIL_FROM = config("MAIL_FROM",cast=str)
    MAIL_PORT = config("MAIL_PORT", cast=int)
    MAIL_SERVER = config("MAIL_SERVER", cast=str)
    MAIL_TLS = config("MAIL_TLS", cast=bool)
    MAIL_SSL = config("MAIL_SSL", cast=bool) 

    DATABASE_URL = config(
        "DATABASE_URL",
        default=f"asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}",
    )
