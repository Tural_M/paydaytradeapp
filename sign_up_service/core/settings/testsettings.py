from starlette.config import Config
from starlette.datastructures import Secret,URL
from core.settings.settings import BaseConfig


class TestSettings(BaseConfig):

    """ Configuration class for site testing environment """

    config = Config()

    HOST = config("EMAIL_CONFIRM_HOST",default="http://127.0.0.1:8000")

    AUTH_SERVICE_HOST = config("AUTH_SERVICE_HOST", default="http://localhost")
    AUTH_SERVICE_PORT = config("AUTH_SERVICE_PORT", cast=int, default=8020)
    AUTH_SERVICE = f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}"

    MAIL_USERNAME = config("MAIL_USERNAME", cast=str, default="sebuhi.sukurov.sh@gmail.com")
    MAIL_PASSWORD = config("MAIL_PASSWORD", cast=str,default="jjhuacxnagzjeijm")
    MAIL_FROM = config("MAIL_FROM",cast=str, default="sebuhi.sukurov.sh@gmail.com")
    MAIL_PORT = config("MAIL_PORT", cast=int,default=587)
    MAIL_SERVER = config("MAIL_SERVER", cast=str, default="smtp.gmail.com")
    MAIL_TLS = config("MAIL_TLS", cast=bool, default=True)
    MAIL_SSL = config("MAIL_SSL", cast=bool, default=False) 

    DEBUG = config("DEBUG", cast=bool, default=True)
    DB_USER = config("DB_USER", cast=str,)
    DB_PASSWORD = config("DB_PASSWORD", cast=Secret)
    DB_HOST = config("DB_HOST", cast=str)
    DB_PORT = config("DB_PORT", cast=str)
    DB_NAME = config("DB_NAME", cast=str)

    DATABASE_URL = config(
        "DATABASE_URL",
        default=f"asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}",
    )
  