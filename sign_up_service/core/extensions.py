import os
import coloredlogs
import logging
from gino.ext.starlette import Gino
from fastapi_mail import ConnectionConfig
from ssl import create_default_context
from passlib.context import CryptContext
from core.factories import settings


if not settings.DEBUG:
    ssl_object =create_default_context(cafile=settings.SSL_CERT_FILE)


    db: Gino = Gino(dsn=settings.DATABASE_URL,ssl=ssl_object,pool_min_size=3,pool_max_size=20,retry_limit=1,retry_interval=1)
else:

    db: Gino = Gino(dsn=settings.DATABASE_URL)


pwd_context: CryptContext = CryptContext(
    schemes="sha256_crypt", sha256_crypt__min_rounds=305945
)

    
def get_logger(log_level="DEBUG"):
    if not settings.DEBUG:
        logger = logging.getLogger("gunicorn.error")
        logger.setLevel(log_level)   

    else:
        logger = logging.getLogger()
        colors_config = coloredlogs.DEFAULT_LEVEL_STYLES
        coloredlogs.DEFAULT_LOG_FORMAT = '%(asctime)s %(hostname)s %(name)s %(levelname)s %(message)s'
        colors_config.update(**{'info': {"color": "white","faint":True}})
        logger.setLevel(log_level)
        coloredlogs.install(level=log_level,logger=logger)

    return logger



log = get_logger(os.environ.get("LOG_LEVEL","DEBUG"))


email_conf = ConnectionConfig(MAIL_USERNAME=settings.MAIL_USERNAME,
                        MAIL_PASSWORD=settings.MAIL_PASSWORD,
                        MAIL_FROM=settings.MAIL_FROM,
                        MAIL_PORT=settings.MAIL_PORT,
                        MAIL_SERVER=settings.MAIL_SERVER,
                        MAIL_TLS=settings.MAIL_TLS,
                        MAIL_SSL=settings.MAIL_SSL)
