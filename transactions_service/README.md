# Python FastAPI Auth Service 



## Run tests ##
```sh
> export TEST_DB_NAME=postgres
> export TEST_DB_PORT=5433
> export TEST_DB_PASSWORD=test123
> export TEST_DB_USER=postgres
> export TEST_DB_HOST=localhost
> export settings=test

> pytest --cov-report term --cov=app tests/
```

## Usage Examples ##


```sh
> pipenv shell or python3 -m venv .ven 
> pipenv install or pip install -r requirements.txt
> export settings=dev
> export DB_NAME=db_name
> export DB_PORT=5432
> export DB_PASSWORD=dbpass
> export DB_USER=username
> export DB_HOST=127.0.0.1

> uvicorn app.main:app --reload --port 8004
```
###### after app is runing, go to http://127.0.0.1:8004/docs to see docs
