import httpx
from core.extensions import log
from core.extensions import db
from app.controllers.controller.schemas import CashSchema
from app.controllers.controller.schemas import CashSchemaDB
from app.data.models import Balance
from core.factories import settings


def clean_dict(data):
    return {key:val for (key,val) in data.items() if val is not None }

def extract_token(data: str) -> str:
    """ Extract JWT token from header """
    if len(data.split()) >=2:
        return data.split()[1]
  

async def check_token(token):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                f"{settings.AUTH_SERVICE}/token/generator/identity",
                headers={"Authorization": f"Bearer {token}"},
            )
            if response.status_code == 200:
                return response.json().get("identity")
            return False

    except Exception as err:
        log.error(f"Error on check_token function ->  {err}")
        return False


async def increase_cash(data: CashSchema, token: str):
    """ increase cash in balance """
    try:
        token = extract_token(token)
        user_id = await check_token(token)
        if not user_id:
            return False

        async with db.transaction():
            balance = await Balance.query.where(Balance.user_id == user_id).gino.first()
            if balance:
                await balance.update(cash = balance.cash + data.cash).apply()
                return CashSchemaDB.from_orm(balance)
            data = data.dict()
            data["user_id"] = user_id
            new_balance = await Balance.create(**data)
            return CashSchemaDB.from_orm(new_balance)
    except Exception as err:
        log.error(f"Error on increase_cash function ->  {err}")
        return False


async def decrease_cash(data: CashSchema, token: str):
    try:
        token = extract_token(token)
        user_id = await check_token(token)
        if not user_id:
            return False

        async with db.transaction():
            balance = await Balance.query.where(Balance.user_id == user_id).gino.first()
            if balance:
                await balance.update(cash = balance.cash - data.cash).apply()
                return CashSchemaDB.from_orm(balance)

        return False
    except Exception as err:
        log.error(f"Error on decrease_cash function ->  {err}")
        return False
