from core.dbsetup import (
    Column,
    Model,
    Float,
    UUIDType
)
from uuid import uuid4


class Balance(Model):
    __tablename__ = "account_balance"

    cash = Column(Float(), nullable=False, default=0.0)
    user_id = Column(UUIDType(),nullable=False)