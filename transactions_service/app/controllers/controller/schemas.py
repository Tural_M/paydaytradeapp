# write your schemas in this files. Use pydantic

import pydantic.json
import asyncpg.pgproto.pgproto
from pydantic import BaseModel
from uuid import UUID
from typing import Union
from datetime import datetime

pydantic.json.ENCODERS_BY_TYPE[asyncpg.pgproto.pgproto.UUID] = str


class ErrorSchema(BaseModel):
    msg: str
    class Config:
        schema_extra = {
             'example': {
                'msg': "message"
              
            }
        }


class CashSchema(BaseModel):
    cash: float


class CashSchemaDB(CashSchema):
    id: UUID
    user_id: UUID
    created: datetime
    updated: Union[datetime,None] = None

    class Config:
        orm_mode = True