import httpx
from fastapi import APIRouter
from fastapi import Header
from starlette.responses import JSONResponse
from core.factories import settings
from app.controllers.controller.schemas import ErrorSchema
from app.controllers.controller.schemas import CashSchema
from app.utils.helpers import increase_cash
from app.utils.helpers import decrease_cash


router = APIRouter()


@router.post("/cash/increase",tags=["Balance"],
response_description="test",
description="test",
include_in_schema=settings.INCLUDE_SCHEMA,
responses={404: {"model": ErrorSchema}})
async def add_cash(data: CashSchema, Authorization: str = Header(None)) -> JSONResponse:
    result = await increase_cash(data, Authorization)
    if result:
        return result
    return JSONResponse({"result" : False})

@router.put("/cash/decrease",tags=["Balance"],
response_description="test",
description="test",
include_in_schema=settings.INCLUDE_SCHEMA,
responses={404: {"model": ErrorSchema}})
async  def update_cash(data: CashSchema, Authorization: str = Header(None))-> JSONResponse:
    result = await decrease_cash(data, Authorization)
    if result:
        return result
    return JSONResponse({"result" : False})