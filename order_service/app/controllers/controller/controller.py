from fastapi import APIRouter
from fastapi import Header
from fastapi import Path
from starlette.responses import JSONResponse
from http import HTTPStatus
from core.factories import settings
from app.controllers.controller.schemas import Buying, Sale
from app.utils.helpers import (buy_ticker, sale_ticker, update_tickets,
                               get_users_stocks, delete_tickets)

router = APIRouter()


@router.post(
    "/users/buy",
    tags=["stocks"],
    response_description="",
    include_in_schema=settings.INCLUDE_SCHEMA,
)
async def stocks_buying(
    data: Buying, Authorization: str = Header(...)) -> JSONResponse:
    result = await buy_ticker(data.dict(), Authorization)

    if result:
        return result

    return JSONResponse(content={
        "result": False,
        "msg": "Failure",
    }, status_code=HTTPStatus.NOT_FOUND)


@router.post(
    "/users/sale",
    tags=["stocks"],
    response_description="",
    include_in_schema=settings.INCLUDE_SCHEMA,
)
async def stocks_sale(
    data: Sale, Authorization: str = Header(...)) -> JSONResponse:
    result = await sale_ticker(data.dict(), Authorization)

    if result:
        return result

    return JSONResponse(content={
        "result": False,
        "msg": "Failure"
    },status_code=HTTPStatus.NOT_FOUND)


@router.get(
    "users/stocks",
    tags=["stocks"],
    response_description="",
    include_in_schema=settings.INCLUDE_SCHEMA,
)
async def check_prices(Authorization: str = Header(...)) -> JSONResponse:

    result = await get_users_stocks(Authorization)

    if result:
        return result

    return JSONResponse(content={
        "result": False,
        "msg": "Failure"
    },status_code=HTTPStatus.NOT_FOUND)


@router.put(
    "users/stocks/{id}",
    tags=["stocks"],
    response_description="",
    include_in_schema=settings.INCLUDE_SCHEMA,
)
async def update(Authorization: str = Header(...),
                 id=Path(..., title="user id")) -> JSONResponse:

    result = await update_tickets(Authorization, id)

    if result:
        return result

    return JSONResponse(content={
        "result": False,
        "msg": "Failure"
    },status_code=HTTPStatus.NOT_FOUND)


@router.delete(
    "users/stocks/{id}",
    tags=["stocks"],
    response_description="",
    include_in_schema=settings.INCLUDE_SCHEMA,
)
async def delete(Authorization: str = Header(...),
                 id=Path(..., title="user id")) -> JSONResponse:

    result = await delete_tickets(Authorization, id)

    if result:
        return result

    return JSONResponse(content={
        "result": False,
        "msg": "Failure"
    },status_code=HTTPStatus.NOT_FOUND)
