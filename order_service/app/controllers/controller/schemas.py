import pydantic.json
import asyncpg.pgproto.pgproto
from datetime import datetime
from uuid import UUID
from core.factories import settings
from pydantic import BaseModel

pydantic.json.ENCODERS_BY_TYPE[asyncpg.pgproto.pgproto.UUID] = str


class Buying(BaseModel):
    ticker: str
    target_price: float


class Sale(Buying):
    ...

