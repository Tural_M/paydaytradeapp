import random
import string
import time
from fastapi import FastAPI
from starlette_prometheus import metrics, PrometheusMiddleware
from app.controllers.controller.controller import router
from starlette.middleware.cors import CORSMiddleware
from core.factories import settings
from core.extensions import db
from core.extensions import log
from starlette.requests import Request
from app.data.models import Stocks
from fastapi.openapi.utils import get_openapi


app = FastAPI()
db.init_app(app)


@app.middleware("http")
async def log_requests(request: Request, call_next):
    idem = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
    log.info(f"RID={idem} START REQUEST PATH={request.url.path} METHOD={request.method} ")
    start_time = time.time()
    
    response = await call_next(request)
    
    process_time = (time.time() - start_time) * 1000
    formatted_process_time = '{0:.2f}'.format(process_time)
    log.info(f"RID={idem} COMPLETED={formatted_process_time}ms REQUEST={request.method.upper()} {request.url.path} STATUS_CODE={response.status_code}")
    
    return response


def modify_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Order Service",
        version="1.0.0",
        routes=app.routes,
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = modify_openapi

@app.on_event("startup")
async def startup():
    print("startapp")

@app.on_event("shutdown")
async def shutdown():
    print("SHUTDOWN")


cors_origins = [i.strip() for i in settings.CORS_ORIGINS.split(",")]
app.add_middleware(
        CORSMiddleware,
        allow_origins="*",
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics/",metrics)
app.include_router(router)


