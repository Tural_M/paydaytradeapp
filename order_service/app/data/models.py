from core.dbsetup import (Column, Model, UUIDType, relationship, String,
                          Integer, ForeignKey, BOOLEAN, Datetime, Date, Text,Float,
                          func)


class Stocks(Model):
    __tablename__ = "stocks"

    open = Column(Float())
    high = Column(Float())
    low = Column(Float())
    close = Column(Float())
    volume = Column(Float())
    adj_high = Column(Float())
    adj_low = Column(Float())
    adj_close = Column(Float())
    adj_open = Column(Float())
    adj_volume = Column(Float())
    symbol = Column(String())
    date = Column(String())




class BuyingStocks(Model):
    __tablename__ = "buying_stocks"
    
    stocks = Column(UUIDType(),ForeignKey("stocks.id",use_alter=True,ondelete="SET NULL"),nullable=False)
    user_id = Column(UUIDType())
    target_price= Column(Float())



class SaleStocks(Model):
    __tablename__ = "sale_stocks"

    user_id = Column(UUIDType())
    stocks = Column(UUIDType(),ForeignKey("stocks.id",use_alter=True,ondelete="SET NULL"),nullable=False)
    target_price =  Column(Float())


class PurchasedStocks(Model):
    __tablename__ = "purchased_stocks"

    user_id = Column(UUIDType())
    stocks = Column(UUIDType(),ForeignKey("stocks.id",use_alter=True,ondelete="SET NULL"),nullable=False)
    target_price =  Column(Float())

