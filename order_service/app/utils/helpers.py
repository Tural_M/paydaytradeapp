import httpx
from core.extensions import db
from core.extensions import log
from core.factories import settings
from app.data.models import Stocks
from app.data.models import SaleStocks
from app.data.models import BuyingStocks
from app.data.models import PurchasedStocks
from sqlalchemy.sql import and_


async def buy_ticker(data, Authorization):
    try:
        identity = await get_identity(Authorization)
        if not identity: return

        result = await check_ticker(data.get("ticker"))
        if not result: return

        return await place_buy_order(data, identity)
    except Exception as er:
        log.error(f"Error on buy_ticker function ->  {er}")
        return False


async def sale_ticker(data, Authorization):
    try:
        identity = await get_identity(Authorization)

        if not identity: return
        result = await check_ticker(data.get("ticker"))
        if not result: return

        return await place_sale_order(data, identity)
    except Exception as er:
        log.error(f"Error on sale_ticker function ->  {er}")
        return False


async def place_sale_order(data, identity):
    try:
        async with db.transaction():
            stock = await Stocks.query.where(
                Stocks.tickers == data.get("ticker")).gino.first()

            if stock.high == data.get("target_price"):
                return await fill_the_order(stock, data, identity)
            buying = await SaleStocks.query.where(
                and_(SaleStocks.stocks == data.get("ticker"),
                     SaleStocks.user_id == identity)).gino.first()
            if buying:
                await buying.update(target_price=data.get("ticker")).apply()
                return True
            else:
                await SaleStocks.create(stocks=stock.id,
                                        user_id=identity,
                                        ticker=data.get("ticker"),
                                        target_price=data.get("target_price"))
                return True
    except Exception as er:
        log.error(f"Error on place_buy_order function ->  {er}")
        return False


async def place_buy_order(data, identity):
    try:
        async with db.transaction():
            stock = await Stocks.query.where(
                Stocks.tickers == data.get("ticker")).gino.first()
            if stock.high == data.get("target_price"):
                return await fill_the_order(stock, data, identity, buy=True)
            buying = await BuyingStocks.query.where(
                and_(BuyingStocks.stocks == data.get("ticker"),
                     BuyingStocks.user_id == identity)).gino.first()
            if buying:
                await buying.update(target_price=data.get("ticker")).apply()
                return True
            else:
                await BuyingStocks.create(
                    stocks=stock.id,
                    user_id=identity,
                    ticker=data.get("ticker"),
                    target_price=data.get("target_price"))
                return True
    except Exception as er:
        log.error(f"Error on place_buy_order function ->  {er}")
        return False


async def fill_the_order(stock, data, identity, buy=False):
    try:
        async with db.transaction():
            if buy:
                buying = await BuyingStocks.query.where(
                    and_(BuyingStocks.stocks == data.get("ticker"),
                         BuyingStocks.user_id == identity)).gino.first()
                if buying:
                    await PurchasedStocks.create(
                        user_id=identity,
                        stock=stock.id,
                        target_price=data.get("target_price"))
                    await send_mail_order_filled(identity)
                    return True
                else:
                    await BuyingStocks.create(
                        stocks=stock.id,
                        user_id=identity,
                        ticker=data.get("ticker"),
                        target_price=data.get("target_price"))
                    await PurchasedStocks.create(
                        user_id=identity,
                        stock=stock.id,
                        target_price=data.get("target_price"))
                    await send_mail_order_filled(identity)
                    return True
            else:
                sale = await SaleStocks.query.where(
                    and_(SaleStocks.stocks == data.get("ticker"),
                         SaleStocks.user_id == identity)).gino.first()
                if sale:
                    await PurchasedStocks.create(
                        user_id=identity,
                        stock=stock.id,
                        target_price=data.get("target_price"))
                    await send_mail_order_filled(identity)
                    return True
                else:
                    await SaleStocks.create(
                        stocks=stock.id,
                        user_id=identity,
                        ticker=data.get("ticker"),
                        target_price=data.get("target_price"))
                    await PurchasedStocks.create(
                        user_id=identity,
                        stock=stock.id,
                        target_price=data.get("target_price"))
                    await send_mail_order_filled(identity)
                    return True

    except Exception as err:
        log.error(f"Error on fill_the_order function ->  {err}")
        return False


async def check_ticker(symbol):
    try:
        async with db.transaction():
            ticker = await Stocks.query.where(Stocks.symbol == symbol
                                              ).gino.first()
            if ticker:
                return True
            else:
                return await new_ticker(symbol)
    except Exception as er:
        log.error(f"Error on check_ticker function ->  {er}")
        return False


async def new_ticker(symbol):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                f"{settings.STOCKS_SERVICE}/stocks/{symbol}")
            if response.status_code == 200:
                async with db.transaction():
                    for value in response.json().values():
                        stock = await Stocks.create(**value)
                        if stock: return True
                        return False
            else:
                return False
    except Exception as err:
        log.error(f"Error on new_ticker function ->  {err}")
        return False


async def get_identity(token):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                f"{settings.AUTH_SERVICE}/token/generator/identity",
                headers={"Authorization": f"Bearer {token}"},
            )
            if response.status_code == 200:
                return response.json()
    except Exception as err:
        log.error(f"Error on save_user function ->  {err}")


async def send_mail_order_filled(identity):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.post(
                f"{settings.REGISTER_SERVICE}/notification", json={"email": identity})
            if response.status_code == 200:
                pass
            else:
                pass
    except Exception as err:
        log.error(f"Error on save_user function ->  {err}")

        return False


async def get_users_stocks(Authorization):
    pass


async def delete_tickets(Authorization, id):
    pass


async def update_tickets(Authorization, id):
    pass
