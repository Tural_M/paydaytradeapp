import os
from starlette.config import Config
from starlette.datastructures import Secret,URL
from core.settings.settings import BaseConfig

class ProdSettings(BaseConfig):

    """ Configuration class for site prodaction environment """

    config = Config()

    DB_USER = config("DB_USER", cast=str)
    DB_PASSWORD = config("DB_PASSWORD", cast=Secret)
    DB_HOST = config("DB_HOST", cast=str)
    DB_PORT = config("DB_PORT", cast=str)
    DB_NAME = config("DB_NAME", cast=str)
    INCLUDE_SCHEMA=config("INCLUDE_SCHEMA", cast=bool, default=False)
    SSL_CERT_FILE = config("SSL_PATH")
    
    STOCKS_SERVICE_HOST = config("STOCKS_SERVICE_HOST")
    STOCKS_SERVICE_PORT = config("STOCKS_SERVICE_PORT")
    STOCKS_SERVICE = f"{STOCKS_SERVICE_HOST}:{STOCKS_SERVICE_PORT}"

    AUTH_SERVICE_HOST = config("AUTH_SERVICE_HOST")
    AUTH_SERVICE_PORT = config("AUTH_SERVICE_PORT")
    AUTH_SERVICE = f"{AUTH_SERVICE_HOST}:{AUTH_SERVICE_PORT}"

    REGISTER_SERVICE_HOST = config("REGISTER_SERVICE_HOST")
    REGISTER_SERVICE_PORT = config("REGISTER_SERVICE_PORT")
    REGISTER_SERVICE = f"{REGISTER_SERVICE_HOST}:{REGISTER_SERVICE_PORT}"

    
    DATABASE_URL = config(
        "DATABASE_URL",
        default=f"asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}",
    )
